/*
To do:
You are given a positive integer 𝑥. Find any such 2 positive integers 𝑎 and 𝑏 such that 𝐺𝐶𝐷(𝑎,𝑏)+𝐿𝐶𝑀(𝑎,𝑏)=𝑥.
As a reminder, 𝐺𝐶𝐷(𝑎,𝑏) is the greatest integer that divides both 𝑎 and 𝑏. Similarly, 𝐿𝐶𝑀(𝑎,𝑏) is the smallest integer such that both 𝑎 and 𝑏 divide it.
It's guaranteed that the solution always exists. If there are several such pairs (𝑎,𝑏), you can output any of them.
*/


#include <bits/stdc++.h>
using namespace std;
int main()
{
    int t;
    scanf("%d",&t);
    while (t--)
    {
        int x;
        scanf("%d",&x);
        printf("1 %d\n",x-1);
    }
}
//It's really 1k IQ solution
