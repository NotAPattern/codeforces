/*
Input

The only line of the input contains a 7-digit hexadecimal number. The first "digit" of the number is letter A, the rest of the "digits" are decimal digits 0-9.
Output

Output a single integer.
https://codeforces.com/contest/1331/problem/D
*/
#include <iostream>
    
int main(){
    std::string a;
    std::cin >> a;
    if(a[a.length() - 1] % 2 == 0){
        std::cout << "0";
    } else{
        std::cout << "1";
    }
    return 0;
    
}