/*
You have 𝑛 students under your control and you have to compose exactly two teams consisting of some subset of your students. Each student had his own skill, the 𝑖-th student skill is denoted by an integer 𝑎𝑖

(different students can have the same skills).

So, about the teams. Firstly, these two teams should have the same size. Two more constraints:

    The first team should consist of students with distinct skills (i.e. all skills in the first team are unique).
    The second team should consist of students with the same skills (i.e. all skills in the second team are equal). 

Note that it is permissible that some student of the first team has the same skill as a student of the second team.

Consider some examples (skills are given):

    [1,2,3]

, [4,4]
is not a good pair of teams because sizes should be the same;
[1,1,2]
, [3,3,3]
is not a good pair of teams because the first team should not contain students with the same skills;
[1,2,3]
, [3,4,4]
is not a good pair of teams because the second team should contain students with the same skills;
[1,2,3]
, [3,3,3]
is a good pair of teams;
[5]
, [6]

    is a good pair of teams. 

Your task is to find the maximum possible size 𝑥
for which it is possible to compose a valid pair of teams, where each team size is 𝑥

(skills in the first team needed to be unique, skills in the second team should be the same between them). A student cannot be part of more than one team.

You have to answer 𝑡

independent test cases.
Input

The first line of the input contains one integer 𝑡
(1≤𝑡≤104) — the number of test cases. Then 𝑡

test cases follow.

The first line of the test case contains one integer 𝑛
(1≤𝑛≤2⋅105) — the number of students. The second line of the test case contains 𝑛 integers 𝑎1,𝑎2,…,𝑎𝑛 (1≤𝑎𝑖≤𝑛), where 𝑎𝑖 is the skill of the 𝑖

-th student. Different students can have the same skills.

It is guaranteed that the sum of 𝑛
over all test cases does not exceed 2⋅105 (∑𝑛≤2⋅105

).
Output

For each test case, print the answer — the maximum possible size 𝑥
for which it is possible to compose a valid pair of teams, where each team size is 𝑥.
https://codeforces.com/contest/1335/problem/C?locale=en
*/
#include <iostream>
#include <set>
#include <algorithm>
#include <map>

using namespace std;
int main(){
int t;
int n, k;
multiset<int> a;
set<int> b;
map<int, size_t> map;
cin >> t;
while(t--){

    cin >> n;

    for(int i = 0; i < n; i++) {
        cin >> k;
        a.insert(k);
        b.insert(k);
    }

    for (auto& r : a) map[r]++;

    size_t max = 0;
    for (auto& r : map) {
        if (r.second > max) max = r.second;
    }

    if(a.size() == 1){
        cout << 0 << endl;
    } else {
        if (max > b.size()) {
            cout << b.size() << endl;
        }
        if (max < b.size()) {
            cout << max << endl;
        }
        if(max == b.size()){
            cout << b.size() - 1 << endl;
        }
    }
    map.clear();
    a.clear();
    b.clear();
}
return 0;
}

//algorithm complexity ~O(n^2*log(n))