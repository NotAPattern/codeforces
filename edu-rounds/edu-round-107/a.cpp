#include <iostream>

using namespace std;
int main() {
  int t, n, r, count;
  cin >> t;
  while(t--) {
    count = 0;
    cin >> n;
    for(int i = 0; i < n; i++){
      cin >> r;
      if(r == 1)
        count++;
      if(r == 3)
        count++;
    }
    cout << count << endl;
  }
  return 0;
}
