#include <iostream>
#include <cmath>
#include <algorithm>
using namespace std;
int main() {
  int t, xa, ya, xb, yb, xf, yf, result;
  cin >> t;
  while (t--) {
    cin >> xa;
    cin >> ya;
    cin >> xb;
    cin >> yb;
    cin >> xf;
    cin >> yf;
	if(xa > xb) {
		swap(xa, xb);
		swap(ya, yb);
	} else if(ya > yb) {
		swap(xa, xb);
		swap(ya, yb);	
	} 
	if((xa == xf || ya == yf || xb == xf || yb == yf) && ((xa < xf && xf < xb) || (ya < yf && yf < yb))) {
		result = std::abs(xa - xb) + std::abs(ya - yb) + 2;
	} else {
		result = std::abs(xa - xb) + std::abs(ya - yb);
	}
	cout << result << endl;
  }
  return 0;
}
