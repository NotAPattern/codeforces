/*
To do:
Ehab has an array 𝑎 of length 𝑛. He has just enough free time to make a new array consisting of 𝑛 copies of the old array, written back-to-back. What will be the length of the new array's longest increasing subsequence?
A sequence 𝑎 is a subsequence of an array 𝑏 if 𝑎 can be obtained from 𝑏 by deletion of several (possibly, zero or all) elements. The longest increasing subsequence of an array is the longest subsequence such that its elements are ordered in strictly increasing order.
*/

#include <bits/stdc++.h>
using namespace std;
int main()
{
    int t;
    scanf("%d",&t);
    while (t--)
    {
        int n;
        scanf("%d",&n);
        set<int> s;
        while (n--)
        {
            int a;
            scanf("%d",&a);
            s.insert(a);
        }
        printf("%d\n",s.size());
    }
}
