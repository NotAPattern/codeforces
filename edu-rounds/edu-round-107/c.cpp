//
// Created by main on 12.04.2021.
//
#include <iostream>
#include <map>

using namespace std;
int main() {
  int q, n, input, pos;
  map<int, int> positon;
  cin >> n;
  cin >> q;
  for (int i = 1; i <= n; i++) {
    cin >> input;
    if (positon[input] == NULL)
      positon[input] = i;
  }
  for (int i = 1; i <= q; i++) {
    cin >> input;
    pos = positon[input];
    cout << pos << " ";
    map<int, int>::iterator it = positon.begin();
    for (it; it != positon.end(); it++) {
      if (it->second < pos)
        (it->second)++;
    }
    positon[input] = 1;
  }
  return 0;
}
