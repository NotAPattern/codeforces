fun main() {
	var len = readLine()!!.toInt()
	for(i in 1..len) {
		val (n, k) = readLine()!!.split(" ").map(String::toInt)
        ('a'..('a' + (k - 1))).forEachIndexed {i, it -> print("$it".repeat(n/k + if((i+1) == k) n%k else 0))}
        println("")
	}
}
