fun main() {
	var len = readLine()!!.toInt()
	for(i in 1..len) {
		val (a, b, k) = readLine()!!.split(" ").map(String::toLong)
        val odd = (k / 2) + (k % 2)
        val notOdd = k - odd
		println(a*odd - b*notOdd)
	}
}
