#include <iostream>

using  namespace std;
int main() {
    int t, n, k;
    cin >> t;
    while(t--){
        cin >> n; cin >> k;
        if(k > 0) {
                if ((2*k+1) <= n) {
                    int max = n;
                    int i = 1;
                    while (i <= max) {
                        if (i <= k) {
                            cout << i << " " << max << " ";
                            max--;
                        } else {
                            cout << i << " ";
                        }
                        i++;
                    }
                } else {
                    cout << "-1";
                }
        } else {
            for(int i = 1; i <= n; i++) {
                cout << i << " ";
            }
        }
        cout << endl;
    }
    return 0;
}
