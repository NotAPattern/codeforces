#include <iostream>
#include <map>

using  namespace std;
int main() {
    int t, n;
	map<char, bool> keys;
	bool sem;
	char input;
		char prev = '0';
    cin >> t;
    while(t--){
		cin >> n;
		sem = true;
		prev = input;
		for(int i = 0; i < n; ++i) {
			cin >> input;
			if(keys.find(input) == keys.end()) { 
				keys[input] = true;
			} else {
				if(prev == input){
					sem = false;
				}
			}
			prev = input;
		}
		if(sem){
			cout << "YES" << endl;
		} else {
			cout << "NO" << endl;
		}
    }
    return 0;
}
