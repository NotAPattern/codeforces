fun main() {
	var len = readLine()!!.toInt()
	for(i in 1..len) {
		val (a, b) = readLine()!!.split(" ").map(String::toInt)
		println(a + b)
	}
}
