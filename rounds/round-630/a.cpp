/*
To do:
 Alice has a cute cat. To keep her cat fit, Alice wants to design an exercising walk for her cat!

Initially, Alice's cat is located in a cell (𝑥,𝑦)

of an infinite grid. According to Alice's theory, cat needs to move:

    exactly 𝑎

steps left: from (𝑢,𝑣) to (𝑢−1,𝑣)
;
exactly 𝑏
steps right: from (𝑢,𝑣) to (𝑢+1,𝑣)
;
exactly 𝑐
steps down: from (𝑢,𝑣) to (𝑢,𝑣−1)
;
exactly 𝑑
steps up: from (𝑢,𝑣) to (𝑢,𝑣+1)

    .

Note that the moves can be performed in an arbitrary order. For example, if the cat has to move 1
step left, 3 steps right and 2

steps down, then the walk right, down, left, right, right, down is valid.

Alice, however, is worrying that her cat might get lost if it moves far away from her. So she hopes that her cat is always in the area [𝑥1,𝑥2]×[𝑦1,𝑦2]
, i.e. for every cat's position (𝑢,𝑣) of a walk 𝑥1≤𝑢≤𝑥2 and 𝑦1≤𝑣≤𝑦2

holds.

Also, note that the cat can visit the same cell multiple times.

Can you help Alice find out if there exists a walk satisfying her wishes?

Formally, the walk should contain exactly 𝑎+𝑏+𝑐+𝑑
unit moves (𝑎 to the left, 𝑏 to the right, 𝑐 to the down, 𝑑 to the up). Alice can do the moves in any order. Her current position (𝑢,𝑣) should always satisfy the constraints: 𝑥1≤𝑢≤𝑥2, 𝑦1≤𝑣≤𝑦2. The staring point is (𝑥,𝑦)

.

You are required to answer 𝑡

test cases independently.
Input

The first line contains a single integer 𝑡
(1≤𝑡≤103

) — the number of testcases.

The first line of each test case contains four integers 𝑎
, 𝑏, 𝑐, 𝑑 (0≤𝑎,𝑏,𝑐,𝑑≤108, 𝑎+𝑏+𝑐+𝑑≥1

).

The second line of the test case contains six integers 𝑥
, 𝑦, 𝑥1, 𝑦1, 𝑥2, 𝑦2 (−108≤𝑥1≤𝑥≤𝑥2≤108, −108≤𝑦1≤𝑦≤𝑦2≤108

).
Output

For each test case, output "YES" in a separate line, if there exists a walk satisfying her wishes. Otherwise, output "NO" in a separate line.

You can print each letter in any case (upper or lower).
https://codeforces.com/contest/1332/problem/A?locale=en
*/


// Code don't pass 3 preset
#include <iostream>

int main() {
    int t;
    int a,b,c,d;
    int x,y,x1,y1,x2,y2;
    std::cin >> t;
    while(t--) {
        std::cin >> a;
        std::cin >> b;
        std::cin >> c;
        std::cin >> d;
        std::cin >> x;
        std::cin >> y;
        std::cin >> x1;
        std::cin >> y1;
        std::cin >> x2;
        std::cin >> y2;
        x = x-a+b;
        y = y-c+d;
        if (x >= x1 && x <= x2 && y >= y1 && y <= y2) {
            if((x1 == x2 && std::abs(-a+b) < x1) || (y1 == y2 && std::abs(-c+d) < y1)){
                //if(((x != x1 || x!= x2) && x1 == x2) || ((y != y1 || y !=y2) && y1 == y2)){
                std::cout << "NO" << std::endl;
            } else {
                std::cout << "YES" << std::endl;
            }
        } else {
            std::cout << "NO" << std::endl;
        }
    }
}

