fun main() {
	val len = readLine()!!.toInt()
	var arr:Array<Int> = readLine()!!.split(" ").map(String::toInt).toTypedArray()
	arr.sort()
    var sum = 0
	for(it in 2..len step 2) {
		sum += arr[it-1] - arr[it-2]
	}
    println(sum)
}
