# Codeforces
Complete (or not) tasks from Codeforces (sponcored by Telegram) rounds.

## File tree
  - `edu-rounds` – rounds from `Educational Codeforces Round`;

  - `rounds` – rounds from `Codeforces Round`;

  - `kotlin-heroes` – rounds from `Kotlin Heroes: Practice/Episode`;

  - `gym` – trainings tasks.

  Other folders – single contests.

## Tasks legend
  - \[X] task – task is complete(all solution)

  - \[~] task – in progress(the solution was close/alien code/what was interesting to me)

  - \[ ] task – don't complete

